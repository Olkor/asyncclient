import queue
import urllib.request
import threading

class Worker(threading.Thread):
    """
        Создаем поток, который будет выполнять
        участок кода, отвечающий за загрузку
        страницы
    """
    def __init__(self, work_queue, client):
        super().__init__()
        self.work_queue = work_queue
        self.client = client # объект клиента должен передаваться

    def run(self):
        while True:
            try:
                url = self.work_queue.get() # получаем url из очереди
                self.process(url, self.client) # обрабатываем этот url
            finally:
                self.work_queue.task_done()

    def process(self, url, client):
        result_dict = {}
        try:
            resource = urllib.request.urlopen(url)
            result_dict['url'] = url
            result_dict['status_code'] = resource.getcode() #получаем код ответа
            result_dict['content'] = resource.read() # получаем контент (т.е. сам html код)
            client.collectionURL.append(result_dict.copy()) # обновляем коллекцию словарей
        except Exception as e:
            result_dict['url'] = url
            result_dict['status_code'] = -1
            result_dict['err'] = e # заносим объект ошибки
            client.collectionURL.append(result_dict.copy())

class AsyncWebPageClient:
    def __init__(self, num_of_threads = 2):
        self.num_of_threads = num_of_threads
        self.collectionURL = []

    def get_pages(self, urls):
        """
            Метод принимает коллекцию url и
            возвращает словарь с кодом ответа,
            url запроса и контентом.
        """
        work_queue = queue.Queue()
        for i in range(self.num_of_threads):
            worker = Worker(work_queue,self)
            worker.daemon = True
            worker.start()
        for url in urls:
            work_queue.put(url)
        work_queue.join()
        return self.collectionURL

